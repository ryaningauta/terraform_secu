provider "azurerm" {
  features {}
}

variable "existing_RG_name" {
  description = "Nom du groupe de ressources Azure."
  type        = string
  default     = "Gr_Ryan"
}

#Groupe de ressources
resource "azurerm_resource_group" "rg" {
  location = "westeurope"
  name     = var.existing_RG_name
}

#Reseau Virtuel 
resource "azurerm_virtual_network" "vnet" {
  name                = "myVNet_RKN"
  address_space       = ["10.21.0.0/16"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "ag_subnet" {
  name                 = "myAGSubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.21.0.0/24"]  # Plage d'adresses pour le sous-réseau de la passerelle d'application
}

resource "azurerm_subnet" "backend_subnet" {
  name                 = "myBackendSubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.21.1.0/24"]  # Plage d'adresses pour le sous-réseau des serveurs back-end
}

resource "azurerm_public_ip" "public_ip" {
  name                = "myAGPublicIPAddress"
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
}


resource "azurerm_application_gateway" "aG" {
  name                = "myAppGateway"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  firewall_policy_id  = azurerm_web_application_firewall_policy.wafpolicy.id

  sku {
    name     = "WAF_v2"
    tier     = "WAF_v2"
    capacity = 2
  }

  gateway_ip_configuration {
    name      = "my-gateway-ip-configuration"
    subnet_id = azurerm_subnet.ag_subnet.id
  }

  frontend_port {
    name = "myAppGateway-frontend_port_name"
    port = 80
  }

  frontend_ip_configuration {
    name                 = "myAppGateway-ip-configuration"
    public_ip_address_id = azurerm_public_ip.public_ip.id
  }

  backend_address_pool {
    name = "myBackendPool"
  }

  backend_http_settings {
    name                  = "myBackendSetting"
    protocol              = "Http"
    cookie_based_affinity = "Disabled"
    port                  = 80
    request_timeout       = 20
  }


  http_listener {
    name                           = "myListener"
    frontend_ip_configuration_name = "myAppGateway-ip-configuration"
    frontend_port_name             = "myAppGateway-frontend_port_name"
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = "myRoutingRule"
    rule_type                  = "Basic"
    priority                   = 10
    http_listener_name         = "myListener"
    backend_address_pool_name  = "myBackendPool"
    backend_http_settings_name = "myBackendSetting"
    
  }
}

#1ere interface réseau
resource "azurerm_network_interface" "nic1" {
  name                = "RKN-nic1"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  
  ip_configuration {
    name = "private1"
    subnet_id = azurerm_subnet.backend_subnet.id 
    private_ip_address_allocation = "Dynamic"
    
  }

}

resource "azurerm_linux_virtual_machine" "vm1" {
  name                = "myVM1_RKN"
  computer_name       = "vm1"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  disable_password_authentication = false
  size                = "Standard_D2s_v4"
  admin_username      = "adminazure"
  admin_password      = "Passw0rd%"

 # availability_set_id   = azurerm_availability_set.availability.id # On met les mêmes sur chaque VM
  network_interface_ids = [
    azurerm_network_interface.nic1.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }
  #Installer Apache + modifier le fichier index 
  custom_data = base64encode(
    <<-EOF
                #!/bin/bash
                sudo apt-get update
                sudo apt-get install -y nginx
                echo "<h1> Welcome to my vm1 </h1>" | sudo tee /var/www/html/index.html
                EOF
  )
}

#2eme interface réseau
resource "azurerm_network_interface" "nic2" {
  name                = "RKN-nic2"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  
  ip_configuration {
    name = "private2"
    subnet_id = azurerm_subnet.backend_subnet.id 
    private_ip_address_allocation = "Dynamic"
    
  }

}

resource "azurerm_linux_virtual_machine" "vm2" {
  name                = "myVM2_RKN"
  computer_name       = "vm2"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = "Standard_D2s_v4"
  disable_password_authentication = false
  admin_username      = "adminazure"
  admin_password      = "Passw0rd%"

  #availability_set_id   = azurerm_availability_set.availability.id # On met les mêmes sur chaque VM
  network_interface_ids = [
    azurerm_network_interface.nic2.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }
  #Installer Apache + modifier le fichier index 
  custom_data = base64encode(
    <<-EOF
                #!/bin/bash
                sudo apt-get update
                sudo apt-get install -y nginx
                echo "<h1> Welcome to my vm2 </h1>" | sudo tee /var/www/html/index.html
                EOF
  )
}

resource "azurerm_network_interface_application_gateway_backend_address_pool_association" "nic1-assoc" {
  network_interface_id    = azurerm_network_interface.nic1.id
  ip_configuration_name   = "private1"
  backend_address_pool_id = one(azurerm_application_gateway.aG.backend_address_pool).id
}

resource "azurerm_network_interface_application_gateway_backend_address_pool_association" "nic2-assoc" {
  network_interface_id    = azurerm_network_interface.nic2.id
  ip_configuration_name   = "private2"
  backend_address_pool_id = one(azurerm_application_gateway.aG.backend_address_pool).id
}


resource "azurerm_web_application_firewall_policy" "wafpolicy" {
  name                = "wafpolicy"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  custom_rules {
    name      = "Rule1"
    priority  = 1
    rule_type = "MatchRule"

    match_conditions {
      match_variables {
        variable_name = "RemoteAddr"
      }

      operator           = "IPMatch"
      negation_condition = false
      match_values       = ["192.168.1.0/24", "10.0.0.0/24"]
    }

    action = "Block"
  }

  custom_rules {
    name      = "Rule2"
    priority  = 2
    rule_type = "MatchRule"

    match_conditions {
      match_variables {
        variable_name = "RemoteAddr"
      }

      operator           = "IPMatch"
      negation_condition = false
      match_values       = ["192.168.1.0/24"]
    }

    match_conditions {
      match_variables {
        variable_name = "RequestHeaders"
        selector      = "UserAgent"
      }

      operator           = "Contains"
      negation_condition = false
      match_values       = ["Windows"]
    }

    action = "Block"
  }

  policy_settings {
    enabled                     = true
    mode                        = "Prevention"
    request_body_check          = true
    file_upload_limit_in_mb     = 100
    max_request_body_size_in_kb = 128
  }

  managed_rules {
    exclusion {
      match_variable          = "RequestHeaderNames"
      selector                = "x-company-secret-header"
      selector_match_operator = "Equals"
    }
    exclusion {
      match_variable          = "RequestCookieNames"
      selector                = "too-tasty"
      selector_match_operator = "EndsWith"
    }

    managed_rule_set {
      type    = "OWASP"
      version = "3.2"
      rule_group_override {
        rule_group_name = "REQUEST-920-PROTOCOL-ENFORCEMENT"
        rule {
          id      = "920300"
          enabled = true
          action  = "Log"
        }

        rule {
          id      = "920440"
          enabled = true
          action  = "Block"
        }
      }
    }
  }
}
